import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { ConfigService } from './config.service';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { catchError } from 'rxjs/internal/operators/catchError';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  baseurl: string = this.configService.getConfiguration().webApiBaseUrl;
  headers = new Headers({ 'Content-Type': 'application/json' }); // ... Set content type to JSON
  options = {
    headers: this.headers
  }; // Create a request option



  get(url: string): Observable<any> {

    return this.http.get(this.baseurl + url)
      .pipe(
        catchError((err) => {
          return this.serviceErrorhandle(err);
        })
      )
  }

  post(url: string, payload: any): Observable<any> {
    const bodyString = JSON.stringify(payload); // Stringify payload

    return this.http.post(this.baseurl + url, bodyString) // ...using post request
      .pipe(
        catchError((err) => {
          return this.serviceErrorhandle(err);
        })
      )
  }

  put(url: string, payload: any): Observable<any> {

    const bodyString = JSON.stringify(payload); // Stringify payload
    return this.http.put(this.baseurl + url, bodyString) // ...using post request
      .pipe(
        catchError((err) => {
          return this.serviceErrorhandle(err);
        })
      )
  }


  delete(url: string): Observable<any> {

    return this.http.get(this.baseurl + url)
      .pipe(
        catchError((err) => {
          return this.serviceErrorhandle(err);
        })
      )

  }

  serviceErrorhandle(err) {
    console.log('error caught in service')
    console.error(err);
    return throwError(err);    //Rethrow it back to component
  }

  constructor(private http: HttpClient, private configService: ConfigService) {
    console.log('http service');
  }
}
