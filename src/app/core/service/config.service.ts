import { Injectable } from '@angular/core';
import { Configuration } from '../models/config';
import { HttpClient } from '@angular/common/http';
import { CacheService } from './cache.service';
import { switchMap } from 'rxjs/operators';
import { pipe } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class ConfigService {

  private config: Configuration;
  constructor(private http: HttpClient, private cacheService: CacheService) { }

  load(url: string) {
    return new Promise<void>((resolve) => {
      let $data = this.http.get(url)
        .pipe(
          switchMap((config: any) => {
            this.config = config;
            console.log('finished loading config');
            return this.cacheService.loadCache(config);

          })
        )
      $data.subscribe(res => {
        resolve();
      })
    });

  }
  getConfiguration(): Configuration {

    return this.config;
  }
}
