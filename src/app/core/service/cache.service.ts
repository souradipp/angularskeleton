import { Injectable } from '@angular/core';
import { Observable, } from 'rxjs';

import { Configuration } from '../models/config';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CacheService {

  private cacheObject: any;

  constructor(private http: HttpClient) { }

  // we need the configuration, but cannot inject the 
  // configService

  loadCache(config: Configuration): Observable<boolean> {
    return new Observable(observer => {
      // load the cache, or whatever we want
      // we cannot use the httpService yet in this APP_INITIALIZERS
      // so we use the basic http servie by angular
      this.http.get(config.webApiBaseUrl + 'api/users?page=2').subscribe(res => { console.log(res) })

      // we need to return an observable
      observer.next(true);
      observer.complete();

    });

  }
}
