import { APP_INITIALIZER, NgModule, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Optional } from '@angular/core';
import { EnsureModuleLoadedOnceGuard } from './ensureModuleLoadedOnceGuard';
import { ConfigService } from './service/config.service';
import { environment } from '@env';
import { CacheService } from './service/cache.service';
import { HttpService } from './service/http.service';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpConfigInterceptor } from './interceptors/http.interceptor';
import { LoaderInterceptor } from './interceptors/loading.interceptor';
import { ApiService } from './service/api/api.service';

export function ConfigLoader(configService: ConfigService) {
  // Note: this factory need to return a function (that return a promise)
  console.log('before load config');
  return () => configService.load(environment.configFile);
}

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [HttpService, ConfigService, CacheService, ApiService,
    { provide: HTTP_INTERCEPTORS, useClass: HttpConfigInterceptor, multi: true },
    { provide: HTTP_INTERCEPTORS, useClass: LoaderInterceptor, multi: true },
    {
      provide: APP_INITIALIZER,
      useFactory: ConfigLoader,
      deps: [ConfigService],
      multi: true
    },
  ]
})
export class CoreModule extends EnsureModuleLoadedOnceGuard {
  constructor(@Optional() @SkipSelf() core: CoreModule) {
    super(core);
  }
}
