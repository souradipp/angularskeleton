import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardhoverDirective } from './directive/cardhover.directive';
import { DateformatPipe } from './pipe/dateformat.pipe';

import * as layout from './layout';
import * as sharedComponent from './components';
import { LoaderComponent } from './components/loader/loader.component'

const layoutComponents = [...layout.layoutComponent];
const sharedComponents = [...sharedComponent.sharedComponents]

@NgModule({
  declarations: [
    layoutComponents,
    sharedComponents,
    CardhoverDirective,
    DateformatPipe,
    LoaderComponent,

  ],
  imports: [
    CommonModule
  ],
  exports: [layoutComponents, sharedComponents]
})
export class SharedModule {
  constructor() {
    console.log("Shared ", [...layout.layoutComponent])
  }
}
