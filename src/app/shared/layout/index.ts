import { FooterComponent } from "./footer/footer.component";
import { HeaderComponent } from "./header/header.component";


export const layoutComponent: any[] = [FooterComponent, HeaderComponent];

export * from "./footer/footer.component";
export * from "./header/header.component"