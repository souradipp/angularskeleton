import { AccordianComponent } from "./accordian/accordian.component";
import { ModalsComponent } from "./modals/modals.component";
import { TablesComponent } from "./tables/tables.component";

export const sharedComponents: any[] = [TablesComponent, AccordianComponent, ModalsComponent];
