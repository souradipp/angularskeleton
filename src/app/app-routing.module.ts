import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from '@core/guards/auth.guard';
import { LoginGuard } from '@core/guards/login.guard';

const routes: Routes = [
  {
    path: "",
    canActivate: [LoginGuard],
    loadChildren: () => import('./features/preauth/preauth.module').then(m => m.PreauthModule)
  },
  {
    path: "dashboard",
    canActivate: [AuthGuard],
    loadChildren: () => import('./features/postauth/postauth.module')
      .then(m => m.PostauthModule)
  }

];

@NgModule({
  imports: [RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
