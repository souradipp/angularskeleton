import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PostauthRoutingModule } from './postauth-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PostauthRoutingModule
  ]
})
export class PostauthModule { }
